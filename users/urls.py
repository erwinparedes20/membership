from django.urls import path

from . import views

urlpatterns = [
    path('', views.user_login, name='user_login'),
    path('user_member', views.user_member, name='user_member'),
    path('add_admin', views.add_admin, name='add_admin'),
    path('staff', views.user_staff, name='user_staff'),
    path('logout', views.user_logout, name='user_logout'),
]

