from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from users.backends import EmailAuthBackend
from members.models import Members
from promotions.models import Promotions, PromotionBenefits
from benefits.models import Benefits

def user_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            if user.is_staff:
                return redirect('user_staff')
            member = Member.objects.get(user=user)
            if member.is_verify == False:
                return render(request,'login.html',{'error':'Usuario aun no ha sido verificado'})
            return redirect('user_member')
        else:
            return render(request,'login.html',{'error':'Usuario o password incorrecto'})
    return render(request, 'login.html')


def user_logout(request):
    logout(request)
    return render(request, 'login.html')

def add_admin(request):
    if request.method == 'POST':
        first_name = request.POST['first-name']
        last_name = request.POST['last-name']
        email = request.POST['email']
        if User.objects.filter(email = email).count():
            error = "Solicitud no anexada"
            mensaje = "El correo electronico ya se encuentra registrado"
            return render(request,'add_admin.html', {'error':error,
                                                      'mensaje':mensaje})
        user = User.objects.create_user(email,email,'redvilagroadmin')
        user.first_name = first_name
        user.last_name = last_name
        user.is_staff = True
        user.is_active = True
        user.save()
        return redirect('user_staff')

    return render(request,'add_admin.html')    


def user_staff(request):
    members = Members.objects.all()
    members_active = Members.objects.filter(is_verify=True)
    number_is_staff = User.objects.filter(is_staff=True).count()
    return render(request,'dashboard.html',{'members':members,'members_active':members_active,
                                            'number_is_staff':number_is_staff})

def user_member(request):
    member = Members.objects.get(user=request.user)
    promotion = Promotions.objects.get(id=member.promotion.id)
    benefits = PromotionBenefits.objects.filter(promotion=promotion.id)
    referraled = Members.objects.filter(referraled_by=member.id)
    free = promotion.number_ref-referraled.count() 
    referraled_active = Members.objects.filter(referraled_by=member.id,is_verify=False)    
    return render(request,'member.html',{'referraled':referraled,'benefits':benefits, 'anexados':referraled.count(),'verificados':referraled_active.count(),'libres':free})
