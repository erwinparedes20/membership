from django.urls import path, include
from benefits.views import benefits,update_benefit

urlpatterns = [
    path('benefits',benefits,name='benefits'),
    path('benefit/<int:id>',update_benefit,name='update_benefit'),
]
