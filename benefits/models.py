from django.db import models

class Benefits(models.Model):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=256,null =True)
    is_active = models.BooleanField(default=True)


