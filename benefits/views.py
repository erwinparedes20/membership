from django.shortcuts import render,redirect
from promotions.models import *
from members.models import Members
from benefits.models import Benefits
from django.contrib.auth.decorators import login_required, permission_required


@login_required
def benefits(request):
    benefits = Benefits.objects.all()
    return render(request,'benefits.html',{'benefits':benefits})


@login_required
def update_benefit(request,id):
    benefit = Benefits.objects.get(pk=id)
    
    if request.method == 'POST':
        benefit.name = request.POST['name']
        benefit.decription = request.POST['description']
        benefit.save()
        return redirect('benefits') 
    promotions = Promotions.objects.filter(promotionbenefits__benefit=benefit)
    members = Members.objects.filter(promotion__in=promotions.values_list('id'))
    return render(request,'update_benefit.html',{'benefit':benefit,
                                                   'id':id,
                                                   'members':members,
                                                   'promotions':promotions})
