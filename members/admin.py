from django.contrib import admin

# Register your models here.
from django.contrib import admin
from members.models import Members

class MembersAdmin(admin.ModelAdmin):
    pass
admin.site.register(Members, MembersAdmin)
