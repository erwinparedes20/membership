import qrcode
from io import StringIO,FileIO,BytesIO,BufferedReader

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from promotions.models import Promotions
from benefits.models import Benefits
from django.urls import reverse
from django.core.files.uploadedfile import InMemoryUploadedFile


class Members(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    promotion = models.ForeignKey(Promotions, null=True, on_delete=models.SET_NULL)
    referraled_by = models.ForeignKey('self', null=True, on_delete=models.SET_NULL)
    referral_counter = models.IntegerField(null=True)
    referral_activies = models.IntegerField(null=True,default=0)
    is_verify = models.BooleanField(default=False)
    codigo_qr = models.ImageField(upload_to='codigos_qr/',null=True)
    inversion = models.DecimalField(max_digits=5, decimal_places=2,default=0)

    def save(self):
        self.generate_qrcode() 
        super(Members, self).save()

#    def get_absolute_url(self):
#        return reverse('members.views.search', args=[str(self.id)])

    def generate_qrcode(self):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=6,
            border=0,
        )
        qr.add_data('http://membresia.redvilagro.com/search/%s' % str(self.id))
        qr.make(fit=True)

        img = qr.make_image()

        filename = 'afiliado-%s.png' % str(self.id)
        from django.conf import settings
        img.save(settings.STATICFILES_DIRS[0] + '/codigos_qr/'+filename,'png')
        #with open(settings.MEDIA_ROOT[0] + '/codigos_qr/'+filename, "rb") as reopen:
        #    archivo = BufferedReader(reopen)
        #    self.codigo_qr.save(filename, archivo, save=True)
        #self.codigo_qr.save(filename,img, save=False)
        

#@receiver(post_save, sender=User)
#def create_user_members(sender, instance, created, **kwargs):
#    if created:
#        Members.objects.create(user=instance)

#@receiver(post_save, sender=User)
#def save_user_members(sender, instance, **kwargs):
#    instance.members.save()


class MembersBenefits(models.Model):
    member = models.ForeignKey(Members, on_delete=models.CASCADE)
    benefit = models.ForeignKey(Benefits, on_delete=models.CASCADE)
    date = models.DateField(blank=True, null=True)

