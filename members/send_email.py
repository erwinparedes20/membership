from django.views.generic import View
from django.utils import timezone
from .models import *
from .render import *
import requests
from threading import Thread, activeCount


def send_email(file: list):
    r = requests.post(
        "https://api.mailgun.net/v3/######/messages",
        auth=("api", "key-########################################"),
        files=[("attachment", (file[0], open(file[1], "rb").read()))],
        data={"from": "No Reply <no-reply@##########>",
              "to": "me@########",
              "subject": "Sales Report",
              "text": "Requested Sales Report",
              "html": "<html>Requested Sales Report</html>"})


class Pdf(View):

    def get(self, request):
        sales = Sales.objects.all()
        today = timezone.now()
        params = {
            'today': today,
            'sales': sales,
            'request': request
        }
        file = Render.render_to_file('pdf.html', params)
        thread = Thread(target=send_email, args=(file,))
        thread.start()
        return HttpResponse("Processed")

