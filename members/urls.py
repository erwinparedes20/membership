from django.urls import path
from wkhtmltopdf.views import PDFTemplateView

from . import views

urlpatterns = [
    path('integrante/<int:id>', views.integrante, name='integrante'),
    path('update_member/<int:id>', views.update_member, name='update_member'),
    path('members_benefit/<int:id>', views.members_benefit, name='members_benefit'),
    path('promotions_benefit/<int:id>', views.promotions_benefit, name='promotions_benefit'),
    path('add_member/', views.add_member, name='add_member'),
    path('pdf/<int:id>', PDFTemplateView.as_view(template_name='afiliacion.html',filename='afiliacion.pdf'), name='pdf'),
    path('afiliacion/<int:id>',views.afiliacion,name='afiliacion'),
]
#path('afiliacion/<int:id>', afiliacion, name='afiliacion'),
