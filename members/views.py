from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.db.models import OuterRef, Subquery
from django.conf import settings
from wkhtmltopdf.views import PDFTemplateView
from django.template.loader import render_to_string
from django.views.generic import View
from django.utils import timezone
from django.contrib.auth.decorators import login_required, permission_required

from .render import Render

from members.models import Members, MembersBenefits
from benefits.models import Benefits
from promotions.models import *
from datetime import date


def afiliacion(request,id):

    filename = 'RedVilagro_afiliacion-%s.pdf' % id
    template_name = 'afiliacion.html'
    cmd_options = {
        'margin-top': 3,
        'size-width': "6cm",
        'size-height': "6cm",

    }
    m = Members.objects.get(pk=id)
    promotion = Promotions.objects.get(id=m.promotion.id)
    codigo_qr = 'http://localhost:8000/static/codigos_qr/afiliado-%s.png' %id
    #codigo_qr = 'http://'+settings.ALLOWED_HOSTS[0]+':8000/search/'+str(id)
    params ={
        'member':m,
        'promotion':promotion,
        'codigo_qr':codigo_qr,
    }
    return Render.render('afiliacion.html', params)
    


def search(request, id):
    member = Members.objects.get(pk=id)
    promotion = Promotions.objects.get(id=member.promotion.id)
    return render(request,'search.html',{'member':member,'promotion':promotion})

@login_required
def member(request):
    member = Members.objects.get(user=request.user)
    promotion = Promotions.objects.get(id=member.promotion.id)
    member_benefits = MembersBenefits.objects.filter(member=member)
    benefits = PromotionBenefits.objects.filter(promotion=promotion.id).exclude(benefit__in=Subquery(member_benefits.values('pk'))) 
    referraled = Members.objects.filter(referraled_by=member.id)
    free = promotion.number_ref-referraled.count() 
    referraled_active = Members.objects.filter(referraled_by=member.id,is_verify=False)
    promotions = Promotions.objects.all()
    if request.method == "POST":
        nombre = request.POST['fname']
        apellido = request.POST['lname']
        email = request.POST['email']
        promocion = Promotions.objects.get(pk=int(request.POST.get('promotion')))
        if User.objects.filter(email=email).count() !=0:
            error = "Solicitud no anexada"
            mensaje = "El correo electronico ya se encuentra registrado"
            return render(request,'members.html',{'referraled':referraled,
                                              'benefits':benefits,
                                              'member_benefits':member_benefits,
                                              'anexados':referraled.count(),
                                              'verificados':referraled_active.count(),
                                              'libres':free,
                                              'promotions':promotions,
                                              'error':error,
                                              'mensaje':mensaje})
        usuario = User.objects.create_user(username=email,
                                     first_name=nombre,
                                     last_name=apellido,
                                     email=email,
                                     password='redvilagro')
        miembro = Members.objects.create(referral_counter  = promotion.number_ref,
                                         referral_activies  =0,
                                         is_verify = False,
                                         promotion = promocion,
                                         referraled_by_id = 1,
                                         user = usuario )
        titulo = "Solicitud anexada"
        mensaje = "El referido fue anexado para su verificacion por parte de Redvilagro"
        return render(request,'members.html',{'referraled':referraled,
                                              'benefits':benefits,
                                              'member_benefits':member_benefits,
                                              'anexados':referraled.count(),
                                              'verificados':referraled_active.count(),
                                              'libres':free,
                                              'promotions':promotions,
                                              'titulo':titulo,
                                              'mensaje':mensaje})
    
    return render(request,'members.html',{'referraled':referraled,
                                          'benefits':benefits, 
                                          'member_benefits':member_benefits,
                                          'anexados':referraled.count(),
                                          'verificados':referraled_active.count(),
                                          'promotions':promotions,
                                          'libres':free})


@login_required
def add_member(request):
    promotions = Promotions.objects.all()
    if request.method == 'POST':
        first_name = request.POST['first-name']
        last_name = request.POST['last-name']
        email = request.POST['email']
        promotion = Promotions.objects.get(pk=int(request.POST.get('promotion')))
        cutoffdates = Cutoffdate.objects.all().order_by('cutoffdate')
        i = 0
        d = date.today()
        for cod in cutoffdates:
            if d >= cod.cutoffdate:
                i +=1
        promotion_cod = PromotionCutoffdate.objects.get(promotion=promotion,cutoffdate=cutoffdates[i])
        error = 'El correo electronico ya existe'
        if User.objects.filter(email = email).count():
            error = "Solicitud no anexada"
            print(error)
            mensaje = "El correo electronico ya se encuentra registrado"
            return render(request,'add_member.html', {'promotions':promotions,
                                                      'error':error,
                                                      'mensaje':mensaje})
        user = User.objects.create_user(email,email,'redvilagro1234')
        user.first_name = first_name
        user.last_name = last_name
        user.save()
        
        member = Members(user = user,
                        promotion = promotion,
                        inversion = promotion_cod.inversion,
                        referral_counter = 0,
                        referral_activies = 0,
                        )
                        #referraled_by = NULL,                       
        member.save()
        return redirect('user_staff')


    return render(request,'add_member.html',{'promotions':promotions})    


@login_required
def integrante(request, id):
    m = Members.objects.get(pk=id)
    referidos = Members.objects.filter(referraled_by=m.id)
    member_benefits = MembersBenefits.objects.filter(member=m.id)
    promotion = Promotions.objects.get(id=m.promotion.id)
    codigo_qr = 'http://'+settings.ALLOWED_HOSTS[0]+'/search/'+str(id)
    print(codigo_qr)
    benefits = PromotionBenefits.objects.filter(promotion=promotion.id).exclude(benefit__in=Subquery(member_benefits.values('pk'))) 
    
    return render(request,'integrante.html',{'member':m,
                                            'id':m.id,
                                            'codigo_qr':codigo_qr,
                                            'referidos':referidos,
                                            'member_benefits':member_benefits,
                                            'benefits':benefits
    })


@login_required
def members_benefit(request,id):
    benefit = Benefits.objects.get(pk=id)
    if request.method == 'POST':
        seleccion = request.POST.get('seleccion')
        print(seleccion)
        arreglo = seleccion.split('*')
        newsmember = Members.objects.filter(id__in=arreglo)
        for member in newsmember:
            mb = MembersBenefits(benefit=benefit,member=member)
            mb.save()
    members = MembersBenefits.objects.filter(benefit=benefit)
    promotions = PromotionBenefits.objects.filter(benefit=benefit).values_list('promotion')
    members_promotions = Members.objects.filter(promotion__in=promotions).exclude(id__in=members.values_list('id'))
    
    return render(request,'members_benefit.html',{'benefit':benefit,
                                                  'members_promotions':members_promotions,
                                                  'members':members,'id':id
                                                  })


@login_required
def promotions_benefit(request,id):
    benefit = Benefits.objects.get(pk=id)
    if request.method == 'POST':
        seleccion = request.POST.get('seleccion')
        print(seleccion)
        arreglo = seleccion.split('*')
        newspromotion = Promotions.objects.filter(id__in=arreglo)
        for promotion in newspromotion:
            mb = PromotionBenefits(benefit=benefit,promotion=promotion)
            mb.save()
    promotions_benefits = PromotionBenefits.objects.filter(benefit=benefit)
    print(promotions_benefits)
    promotions = Promotions.objects.all().exclude(id__in=promotions_benefits.values_list('promotion'))
    print(promotions)
    return render(request,'promotions_benefit.html',{'benefit':benefit,
                                                  'promotions_benefits':promotions_benefits,
                                                  'promotions':promotions,'id':id
                                                  })


@login_required
def update_member(request, id):
    if request.method == 'POST':

        first_name = request.POST['first-name']
        last_name = request.POST['last-name']
        
        member = Members.objects.get(pk=id)
        print(request.POST.get('is_verify'))
        is_verify = True if request.POST.get('is_verify')=="True" else False
        member.is_verify = is_verify
        member.save()
    
        defaults = {'first_name': first_name,
                    'last_name':last_name}

        obj = User.objects.get(pk=member.user.id)
        for key, value in defaults.items():
            setattr(obj, key, value)
        obj.save()
    return redirect('user_staff')

