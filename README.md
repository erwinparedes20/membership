Para instalar la aplicación en modo desarrollo debera seguir los siguientes pasos:
==================================================================================

1-) Instalar el controlador de versiones git:
---------------------------------------------

    Ingresar como super usuario:

    $ su

    # aptitude install git
    
    Salir del modo super usuario


2-) Descargar el codigo fuente del proyecto:
----------------------------------------------------

    Para descargar el código fuente del proyecto contenido en su repositorio GIT realice un clon del proyecto ENVIRTUAL:

    $ git clone https://gitlab.com/erwinparedes20/membership.git


3-) Crear un Ambiente Virtual:
---------------------------------------

    El proyecto está desarrollado con el lenguaje de programación Python, se debe instalar Python v3.7 Con los siguientes comandos puede instalar Python y PIP.

    Entrar como root o super usaurio para la instalacion 

    # aptitude install python3.7 python3-pip python3.7-dev python3-setuptools

    # aptitude install python3-virtualenv virtualenvwrapper

    Salir del modo root y crear el ambiente:

    $ mkvirtualenv --python=/usr/bin/python3 ENVIRTUAL


4-) Instalar los requerimientos del proyecto:
---------------------------------------------

    Para activar el ambiente virtual ENVIRTUAL ejecute el siguiente comando:

    $ workon ENVIRTUAL

    Con el comando anterior se activa el ambiente virtual quedando de la siguiente manera:

    (ENVIRTUAL)$

    Entrar en la carpeta raiz del proyecto:

    (ENVIRTUAL)$ cd membership

    (ENVIRTUAL)membership$ 

    Desde ahi se deben instalar los requirimientos del proyecto con el siguiente comando:

    (ENVIRTUAL)membership$ pip install -r requirements.txt

    De esta manera se instalaran todos los requerimientos iniciales para montar el proyecto


5-) Crear base de datos y Migrar los modelos:
---------------------------------------------
    
    El manejador de base de datos que usa el proyecto es postgres, antes de la creación de la base de datos, se deba asegurar que postgres este instalado correctamente en su equipo.

    Para crear la base de datos desde postgres se ingresa a la consola interactiva de postgres y se ejecuta la siguiente sentencia:

    postgres=# CREATE USER administrador WITH ENCRYPTED PASSWORD '123456';

    postgres=# CREATE DATABASE redvilagromembership OWNER=administrador ENCODING='UTF−8';

    Salir de postgres

    (ENVIRTUAL)membership$ python manage.py makemigrations

    (ENVIRTUAL)membership$ python manage.py migrate

5.1-) Si posee un backup de la base de datos debes realizar los siguientes pasos:
-----------------------------------------------------------------------------------

	* Realizar las migraciones descritas en el paso 5
	* Restaurar la base de datos desde la consola de postgres o con un manejador visual como pgadmin

    Si existe problemas con los indice de la tabla groupModule_usergroup se debe restaurar los indices.

	* Restaurar el indice de la tabla "groupModule_usergroup" con el siguiente sentencia:

		ALTER SEQUENCE "groupModule_usergroup_id_seq" RESTART WITH 439

    En caso contrario seguir con el despliegue de la plataforma.


6-) Inicio de la plataforma:
----------------------------

    (ENVIRTUAL)membership$ python manage.py createsuperuser

    (ENVIRTUAL)membership$ python manage.py runserver
    
    Se despliega la plataforma en el puerto 8000 

    Puede iniciar el navegador de su preferencia e ingresar en la url la siguiente direccion:

    http://localhost:8000

# REDVILAGRO
RedvilagroMembership 1.0.0 (Using codigo QR)
