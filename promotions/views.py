from django.shortcuts import render,redirect
from promotions.models import *
from members.models import Members
from benefits.models import Benefits
from django.contrib.auth.decorators import login_required, permission_required

import datetime


@login_required
def promotions(request):
    promotions = Promotions.objects.all()
    return render(request,'promotions.html',{'promotions':promotions})


@login_required
def update_promotion(request,id):
    promotion = Promotions.objects.get(pk=id)
    members = Members.objects.filter(promotion=promotion)
    benefits = Benefits.objects.filter(promotionbenefits__promotion=promotion)
    return render(request,'update_promotion.html',{'promotion':promotion,
                                                   'id':id,
                                                   'members':members,
                                                   'benefits':benefits})


@login_required
def add_promotion(request):
    if request.method == "POST":
        name = request.POST['name']
        number_ref = request.POST['number_ref']
        promotion = Promotions(name=name,number_ref=number_ref)
        promotion.save()
        return redirect('promotions')
    benefits = Benefits.objects.all()
    return render(request,'add_promotion.html',{'benefits':benefits})


@login_required
def cutsdate(request):
    if request.method == "POST":
        c = request.POST['cutsoffdate']
        d = c.split("/")
        t = datetime.date(int(d[2]),int(d[1]),int(d[0]))
        cutoff = Cutoffdate(cutoffdate=t)
        cutoff.save()
    cutsoffdates = Cutoffdate.objects.all().order_by('cutoffdate')
    return render(request,'cutsdates.html',{'cutsoffdates':cutsoffdates})


@login_required
def delete_cutoffdate(request,id):
    cutoff = Cutoffdate.objects.get(pk=id)
    promotions = PromotionCutoffdate.objects.filter(cutoffdate=cutoff).count()
    if promotions > 0:
        error = 'No se puede eliminar'
        mensaje = 'Existen promociones con la fecha de corte asignada'
        cutsoffdates = Cutoffdate.objects.all().order_by('cutoffdate')
        return render(request,'cutsdates.html',{'cutsoffdates':cutsoffdates,
                                                'titulo':titulo,
                                                'mensaje':mensaje})
    cutoff.delete()

    return redirect('cutsdate')


@login_required
def promotions_cutoffdate(request,id):
    cutoff = Cutoffdate.objects.get(pk=id)
    promotions = PromotionCutoffdate.objects.filter(cutoffdate=cutoff)
    if promotions.count() == 0:
        promo = Promotions.objects.all()
        for p in promo:
            pcd = PromotionCutoffdate(promotion=p,cutoffdate=cutoff,inversion=0)
            pcd.save()

    promotions = PromotionCutoffdate.objects.filter(cutoffdate=cutoff)
    
    if request.method == "POST":
        for p in promotions:
            inversion = request.POST.get('inversion'+str(p.id))
            obj = PromotionCutoffdate.objects.get(pk=p.id)
            obj.inversion = float(inversion)
            obj.save()
        return redirect('cutsdate')

    return render(request,'promotions_cutsdates.html',{'cutoff':cutoff,
                                                       'promotions':promotions,'id':id})

