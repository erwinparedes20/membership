from django.urls import path, include
from promotions.views import *

urlpatterns = [
    path('promotions',promotions,name='promotions'),
    path('promotion/<int:id>',update_promotion,name='update_promotion'),
    path('add_promotion',add_promotion,name='add_promotion'),
    path('cutsdate',cutsdate,name='cutsdate'),
    path('delete_cutoffdate/<int:id>',delete_cutoffdate,name='delete_cutoffdate'),
    path('promotions_cutoffdate/<int:id>',promotions_cutoffdate,name='promotions_cutoffdate'),
]
