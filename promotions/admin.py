from django.contrib import admin

# Register your models here.
from django.contrib import admin
from promotions.models import Promotions

class PromotionsAdmin(admin.ModelAdmin):
    pass
admin.site.register(Promotions, PromotionsAdmin)
