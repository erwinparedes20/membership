from django.db import models
from benefits.models import Benefits


class Promotions(models.Model):
    name = models.CharField(max_length=256)
    number_ref = models.IntegerField(default=0)

class Cutoffdate(models.Model):
    cutoffdate = models.DateField()

class PromotionCutoffdate(models.Model):
    promotion = models.ForeignKey(Promotions, on_delete=models.CASCADE)
    cutoffdate = models.ForeignKey(Cutoffdate, on_delete=models.CASCADE)
    inversion = models.DecimalField(max_digits=5, decimal_places=2,default=0)

class PromotionBenefits(models.Model):
    promotion = models.ForeignKey(Promotions, on_delete=models.CASCADE)
    benefit = models.ForeignKey(Benefits, on_delete=models.CASCADE)

